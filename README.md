# On-demand Environments-as-a-Service

Integrates as an event-driven step in your CI/CD pipeline to manage on-demand, ephemeral test environments for every feature branch. Runs on [Uffizzi Platform](https://uffizzi.com) or your own open source [installation](https://github.com/UffizziCloud/uffizzi_app) on Kubernetes.

## Reusable Pipeline

This repository is an includable, reusable workflow for your Gitlab Pipeline. This can handle creating, updating, and deleting Uffizzi Environments. It will also publish environment URL's within comments on your Slack channel. We recommend using this workflow instead of the individual actions.

### Pipeline Calling Example

This example builds and publishes an image on Docker Hub. It then renders a Uffizzi Docker Compose file from a template and caches it. It then calls the reusable pipeline to create, update, or delete the environment associated with this Merge Request.

```yaml
include:
 - 'https://gitlab.com/uffizzi/environment-action/raw/main/reusable.yaml'

variables:
  DOCKER_REPO_NAME: uffizzitest
  TAG: latest
  IMAGE_NAME: $DOCKER_REPO_NAME/app:$TAG
  COMPOSE_FILE: docker-compose.rendered.yml

.build_image: &build_image
  stage: build
  image: docker:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASSWORD
    - docker build -t $IMAGE $DOCKERFILE_PATH
    - docker push $IMAGE

services:
  - docker:dind

stages:
  - build
  - deploy
  - delete

build_image:
 <<: *build_image
 variables:
   DOCKERFILE_PATH: ./vote
   IMAGE: $IMAGE_NAME

render_compose_file:
  stage: build
  image: alpine:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  before_script:
    - apk add --no-cache gettext
  script:
    - envsubst < ./docker-compose.template.yml > ./docker-compose.rendered.yml
    - echo "$(md5sum docker-compose.rendered.yml | awk '{ print $1 }')"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - ./docker-compose.rendered.yml
```

### Pipeline Inputs

#### `UFFIZZI_USER`

**Required** Uffizzi username

#### `UFFIZZI_PASSWORD`

**Required** Uffizzi password

#### `UFFIZZI_PROJECT`

**Required** Uffizzi project name

#### `UFFIZZI_SERVER`

URL of your Uffizzi installation

#### `CHANNEL`

Slack Channel ID for event notifications

#### `WEBHOOK_URL`

Incoming webhook URL of your Slack channel for event notifications

<https://api.slack.com/messaging/webhooks>

#### `SLACK_TOKEN`

Access OAuth token for your Slack channel with `search:read` & `groups:history` scope

<https://api.slack.com/authentication/token-types>

<https://api.slack.com/legacy/oauth-scopes>

#### `COMPOSE_FILE`

**Required** Path to a compose file within your repository

### Dockerhub Authentication

#### `DOCKERHUB_USER`

Dockerhub username

#### `DOCKERHUB_PASSWORD`

Dockerhub password or auth token
