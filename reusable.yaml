stages:
  - build
  - deploy
  - delete

variables:
  UFFIZZI_SERVER: https://app.uffizzi.com
  DEPLOYMENT_ID: $CI_MERGE_REQUEST_IID
  COMPOSE_FILE: docker-compose.yml
  SLEEP: 5

deploy_environment:
  stage: deploy
  image:
    name: uffizzi/cli:v0.12.0
    entrypoint: [""]
  rules:
    - if: $CI_MERGE_REQUEST_ID
  before_script:
    - apt-get update -qq && apt-get install -y -qq curl jq gettext-base
  script:
    - |
      curl -N --location --request GET "https://slack.com/api/search.messages?channel=$CHANNEL&query=$CI_PROJECT_NAME-MR-$CI_MERGE_REQUEST_IID" \
        --header "Content-Type: application/x-www-form-urlencoded" --header "Authorization: Bearer $SLACK_TOKEN" > output.json
    - |
      if cat output.json | jq -e --arg CI_MERGE_REQUEST_IID "MR-$CI_MERGE_REQUEST_IID" '. | .messages.matches[] | select(. | length > 0) | .blocks[] | select(.text.text | contains($CI_MERGE_REQUEST_IID))| select(. != null)'; then
        ENVIRONMENT_ID=$(cat output.json | jq -e --arg CI_MERGE_REQUEST_IID "MR-$CI_MERGE_REQUEST_IID" '. | .messages.matches[] | select(. | length > 0) | .blocks[] | select(.text.text | contains($CI_MERGE_REQUEST_IID))| .text.text' | awk -F[\*\*] '{print $2}')
        echo "Update $ENVIRONMENT_ID"
        /root/docker-entrypoint.sh preview update --output=json $ENVIRONMENT_ID $COMPOSE_FILE >deploy_output.json
        ENVIRONMENT_URL=$(tail -n 1 deploy_output.json | jq -r '.url')
        echo "Sleeping for $SLEEP seconds before checking status of environment"
        sleep $SLEEP
        curl --silent --show-error --fail $ENVIRONMENT_URL > /dev/null
        echo "Posting notification to Slack"
        curl --location --request POST "${WEBHOOK_URL}" \
        --header 'Content-Type: application/json' \
        --header "Authorization: Bearer ${SLACK_TOKEN}" \
        -d @- <<EOF
      {"blocks":[{"type":"section","text":{"type":"mrkdwn","text":"Uffizzi Environment *${ENVIRONMENT_ID}* was updated\nPreview URL:\n<$ENVIRONMENT_URL>"}}]}
      EOF
      else
        echo "Deploy"
        /root/docker-entrypoint.sh preview create --output=json $COMPOSE_FILE >deploy_output.json
        export ENVIRONMENT_ID=$(tail -n 1 deploy_output.json | jq -e -r '.id')
        export ENVIRONMENT_URL=$(tail -n 1 deploy_output.json | jq -e -r '.url')
        echo "Sleeping for $SLEEP seconds before checking status of environment"
        sleep $SLEEP
        curl --silent --show-error --fail $ENVIRONMENT_URL > /dev/null
        echo "Posting notification to Slack"
        curl --location --request POST "${WEBHOOK_URL}" \
        --header 'Content-Type: application/json' \
        --header "Authorization: Bearer ${SLACK_TOKEN}" \
        -d @- <<EOF 
      {"blocks":[{"type":"section","text":{"type":"mrkdwn","text":"Uffizzi Environment deployed at URL:\n<$ENVIRONMENT_URL>\nUffizzi Environment ID: *${ENVIRONMENT_ID}* \nMerge request: <$CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID|$CI_PROJECT_NAME-MR-$CI_MERGE_REQUEST_IID>"}}]}
      EOF
        fi
    - echo "ENVIRONMENT_ID=$ENVIRONMENT_ID" >> deploy.env
    - echo "DYNAMIC_ENVIRONMENT_URL=$ENVIRONMENT_URL" >> deploy.env
  artifacts:
    reports:
      dotenv: deploy.env
  environment:
    name: "MR-${CI_MERGE_REQUEST_IID}"
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: delete_environment

delete_environment:
  stage: delete
  image:
    name: uffizzi/cli:v0.12.0
    entrypoint: [""]
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  needs:
    - deploy_environment
  script:
    - echo "Delete $ENVIRONMENT_ID"
    - /root/docker-entrypoint.sh preview delete $ENVIRONMENT_ID
    - echo "Posting notification to Slack"
    - |
      curl --location --request POST "${WEBHOOK_URL}" \
      --header 'Content-Type: application/json' \
      --header "Authorization: Bearer ${SLACK_TOKEN}" \
      -d @- <<EOF
      {"blocks":[{"type":"section","text":{"type":"mrkdwn","text":"Uffizzi Environment *${ENVIRONMENT_ID}* deleted"}}]}
      EOF
  environment:
    name: "MR-${CI_MERGE_REQUEST_IID}"
    action: stop
